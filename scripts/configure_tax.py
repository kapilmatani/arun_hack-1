import psycopg2
import sys
import requests

db_host = 'treebo-pricing-master-mum-rds.treebo.com'
db_name = 'taxation'
db_user = 'treebotax'
db_port = '5432'
db_pass  = 'dukDrU4smAuwJZjQ'

try:
    hotel_cs_id = sys.argv[1]
    state = sys.argv[2]

    connection = psycopg2.connect(host=db_host, user=db_user, dbname=db_name, port=db_port, password=db_pass)
    cur = connection.cursor()
    print("Connected to Taxation DB")
    if state != 'kerala':
        psql_select_query = ''' SELECT tax_config_id FROM taxation_hotel_tax_config_mapping WHERE hotel_id = '0377942'; ''' ## Non Kerala hotel query
        cur.execute(psql_select_query)
        mappings = cur.fetchall()
        print('number of mappings is {}'.format(len(mappings)))
    elif state == 'kerala':
        psql_select_query = ''' SELECT tax_config_id FROM taxation_hotel_tax_config_mapping WHERE hotel_id = '1168384'; ''' ## Kerala hotel query
        cur.execute(psql_select_query)
        mappings = cur.fetchall()
        print('number of mappings is {}'.format(len(mappings)))

    psql_insert_query = ''' INSERT INTO taxation_hotel_tax_config_mapping VALUES('{}','{}') '''
    for val in mappings:
        print(psql_insert_query.format(hotel_cs_id,val[0]))
        cur.execute(psql_insert_query.format(hotel_cs_id,val[0]))
    connection.commit()
    print('Completed')

except Exception as e:
    print(e)
    url = 'https://hooks.slack.com/services/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'
    payload = str({"text": "devops-p-taxconfig script on Jenkins failed with error : {}. Please check".format(e)})
    result = requests.post(url, payload)
finally:
    if connection:
        connection.close()
        cur.close()
        print('Closed from Taxation DB')
from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView

from django.contrib import admin


# from create_invoice.views import UploadSheet

urlpatterns = [
     url(r'^', include('apps.hms_searching.urls')),
]
#+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

import requests
import sys
import itertools
import csv
import time
import json


hotel_id = []
stay_start = []
stay_end = []
acacia_post_tax = []
acacia_extra_adult = []
oak_post_tax = []
oak_extra_adult = []
maple_post = []
maple_extra_adult = []
mahagony_post = []
mahagony_extra_adult = []

with open('src/test/java/priceoverride/price_override.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    for line in csv_reader:
        hotel_id.append(line[0])
        stay_start.append(line[1])
        stay_end.append(line[2])
        acacia_post_tax.append(line[3])
        acacia_extra_adult.append(line[4])
        oak_post_tax.append(line[5])
        oak_extra_adult.append(line[6])
        maple_post.append(line[7])
        maple_extra_adult.append(line[8])
        mahagony_post.append(line[9])
        mahagony_extra_adult.append(line[10])


    for (hotelId,fromDate,toDate,room_acacia,room_acaciaAdult,room_oak,room_oakAdult,room_maple,room_mapleAdult,room_mahogany,room_mahoganyAdult) in zip(hotel_id,stay_start,stay_end,acacia_post_tax,acacia_extra_adult,oak_post_tax,oak_extra_adult,maple_post,maple_extra_adult,mahagony_post,mahagony_extra_adult):
        req = {
        "property_code": hotelId,
        "user_id": '3050817',
        "hotel_owner_email": 'nithin.kr@treebohotels.com',
        "stay_start": fromDate,
        "stay_end": toDate,
        "is_post_tax_prices": True,
        "room_details": {
        "acacia": room_acacia,
        "acacia_adult": room_acaciaAdult,
        "oak": room_oak,
        "oak_adult": room_oakAdult,
        "maple": room_maple,
        "maple_adult": room_mapleAdult,
        "mahogany": room_mahogany,
        "mahogany_adult": room_mahoganyAdult,
                },
        }
        print(req)
        req1 = json.dumps(req)
        headers = {'Content-Type': 'application/json'}
        a = requests.post('http://rackrate.treebo.com/v1/rates/indepedent_hotel_rates_override/', data=req1, headers=headers)
        print(a.json())
        time.sleep(1)
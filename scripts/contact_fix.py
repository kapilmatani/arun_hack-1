import csv
import itertools
import os
from time import sleep

c_code = []
c_value = []

with open('scripts/contacts_ups.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    for line in csv_reader:
        c_code.append(line[0])
        c_value.append(line[1])
    
for (ccode,cvalue) in zip(c_code,c_value):
    cmd = ''' curl --location --request POST 'https://notification.treebo.com/v1/notification/register' \
                --header 'Content-Type: application/json' \
                --data '{
                "notification_type": "whatsapp",
                "identifier": "''' + ccode + cvalue + '''" ,
                "source": "ups",
                "message": ""
                }' '''
    os.system(cmd)
    print('\n')
    sleep(0.5)
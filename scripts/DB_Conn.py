import psycopg2


# Define all the DB connections here

class DBConn():
    def __init__(self, app_name):
        self.hostname = ''
        self.username = ""
        self.password = ""
        self.database = ""
        self.set_app_config(app_name)

    # Define all different type of DB coonctions by giving the app name
    def set_app_config(self, app_name):

        if app_name == 'web':
            self.hostname = "direct-p-master.treebo.com"
            self.username = "treeboadmin"
            self.password = "caTwimEx3"
            self.database = "treebo"
            self.port = "5432"

        elif app_name == 'b2b':
            self.hostname = "b2b-master-rds-01.treebo.com"
            self.username = "b2buser"
            self.password = "HSeWxPwFerPTbf9"
            self.database = "b2bapi"
            self.port = "5432"

    def get_connection(self):
        connection = psycopg2.connect(host=self.hostname, user=self.username, password=self.password,
                                      dbname=self.database, port=self.port)
        cur = connection.cursor()
        return cur

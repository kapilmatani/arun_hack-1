import psycopg2,psycopg2.extensions
from datetime import datetime
from datetime import timedelta
import requests

def delete_records_from_table(cur,connection,table):
    print('Deleting from {}'.format(table))

    last_month = datetime.now() - timedelta(days=30)
    postgres_select_query = ''' select count(*) from {} where created_at < '{}' '''.format(table,last_month)
    postgres_delete_query = '''DELETE FROM {} where created_at < '{}' '''.format(table,last_month)

    cur.execute(postgres_select_query)
    number_of_records = cur.fetchall()[0][0]

    cur.execute(postgres_delete_query)
    connection.commit()
    print('Deleted {} records from {}'.format(number_of_records,table))

def vacuum(cur , table):
    postgres_auto_vaccum_query = '''VACUUM ANALYZE {}'''.format(table)
    cur.execute(postgres_auto_vaccum_query)


host_name='rds-crs.treebo.be'
db_name='crs_db'
db_user='crs_user'
db_pass='qtR8T456*wiopj'
dp_port='5432'
try:
    connection = psycopg2.connect(host=host_name, user=db_user, dbname=db_name, port=dp_port, password=db_pass)
    cur = connection.cursor()
    table_names = ['integration_event','payment','room_type_inventory_availability','job','room_allocation','booking_invoice_group',
                   'booking_audit_trail','booking_action','dnr_audit_trail','charge','payment_split','expense','bill','charge_split',
                   'guest_stay','guest_allocation','room_stay','invoice_charges','invoice']
    for table in table_names :
        delete_records_from_table(cur,connection,table)
    connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    for table in table_names:
        vacuum(cur,table)
    print('ran vacuum')
except Exception as e:
    print(e)
    url = 'https://hooks.slack.com/services/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'
    payload = str({"text": "auto_vacuum_crs_dbStage script on Jenkins failed with error : {}. Please check".format(e)})
    result = requests.post(url, payload)

finally:
    if connection:
        connection.close()
        cur.close()
        print('closed from crs_db')

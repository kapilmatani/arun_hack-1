import boto3, os, time

ec2_client = boto3.client('ec2',region_name = 'ap-southeast-1')

def create_lt_version(ami_id,ami_name):
    print('Creating new version for launch templates')
    ami_name_split = ami_name.split('-')
    pod = ami_name_split[0]
    print("Pod: {}".format(pod))
    env = ami_name_split[1]
    print("Env: {}".format(env))
    service = ami_name_split[2]
    print('Service: {}'.format(service))
    
    
    lt_name = pod + '-' + env + '-' + service + '-' + 'lt'
    print("launch template name: {}".format(lt_name))
    if(service == 'vendor'):
        lt_name = pod + '-' + env + '-' + service + '-' +'portal' +'-' + 'lt' #+ '-' + 'new'
    if(service == 'its' and ami_name_split[3] == 'workers'):
        lt_name = pod + '-' + env + '-' + service + '-' +'workers' +'-' + 'lt' #+ '-' + 'new'
    if(service == 'shared' and ami_name_split[3] == 'taxrackratepo'):
        lt_name = pod + '-' + env + '-' + service + '-' + 'taxrackratepo' +'-' #+ 'lt' + '-' + 'new'
    if(service == 'communication'):
        lt_name = pod + '-' + env + '-' + service + '-' + 'service' +'-' + 'lt' #+ '-' + 'new'
    if(service == 'crsapi'):
        lt_name = pod + '-' + env + '-' + service + '-'  + 'authz' +'-' + 'lt' #+ '-' + 'new'
    try:
        lt_describe = ec2_client.describe_launch_templates(LaunchTemplateNames=[
            lt_name],)
        print("Launch Template Describe: {}".format(lt_describe))
        lt_version_describe = ec2_client.describe_launch_template_versions(LaunchTemplateName= lt_name,)
        print("Launch Template Version Describe: {}".format(lt_version_describe))
    
        
        lt_version_update = ec2_client.create_launch_template_version(LaunchTemplateName =lt_name,
        LaunchTemplateData={
        'BlockDeviceMappings': [
            {
                'DeviceName': '/dev/sda1',
                'Ebs': {
                    'Encrypted': True,
                    'DeleteOnTermination': True,
                    'VolumeSize': 50,
                    'VolumeType': 'gp2',
                    'KmsKeyId' : '1edca945-d82f-4cdd-af25-d7405e891f54
'
                },
            },
        ],
        'NetworkInterfaces': [
            {
                'AssociatePublicIpAddress': True,
                'DeleteOnTermination': True,
                'DeviceIndex': 0,
                'Groups': [
                    'sg-0585016d',
                ],
            },
        ],
        'ImageId': ami_id,
    })
    except:
        print("Launch template {} does not exist".format(lt_name))
        
        
def lambda_handler(event, context):
    
    print('Start-Time:-> {}'.format(time.ctime()))
    #time.sleep(30)
    #print('Describing event {}'.format(event))
    try:
        print('Describing event {}'.format(event))
        #instance_id = event['detail']['instance-id']
        ami_describe = ec2_client.describe_images(Owners=['self'])
        print('Describing AMI {}'.format(ami_describe))
        
        for ami in ami_describe['Images']:
            delete_after_check = False
            ami_name = ""
            print('AMI Describe: {}'.format(ami))
            ami_id = ami['ImageId']
            print('AMI Id: {}'.format(ami_id))
            
            try:
                ami_tags = ami['Tags']
                print('AMI Tags: {}'.format(ami_tags))
                for ami_tag in ami_tags:
                    print("AMI Tag: {}".format(ami_tag))
                    ami_key = ami_tag['Key']
                    if(ami_key == 'DeleteAfter'):
                        delete_after_check = True
                    if(ami_key == 'Name'):
                        ami_name = ami_tag['Value']
                        
                        #ami_value = ami_tag['Value']
                        #print("AMI Value: {}".format(ami_value))
                if(delete_after_check == True):
                    print(ami_name)
                    create_lt_version(ami_id,ami_name)
            #ami_name = ami_tags['Value']
            #print("AMI Name: {}".format(ami_name))
            
            except Exception as e:
                print('Error: {}'.format(e))
                
    except Exception as e:
        print('error is {} not found'.format(e))
import psycopg2
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import time
import sys
import os
import re
import requests
from urllib2 import HTTPError
import urllib
import urllib2
import json
import csv
from smtplib import SMTP_SSL as SMTP
from email import Encoders
from email.mime.text import MIMEText



if os.path.exists("proddbsizes.csv"):
  os.remove("proddbsizes.csv")
else:
  print('File does not exists')

sizecomparison=10


#marvin prod db
hostname = 'partner-portal-master-mum-rds.treebo.com'
username = 'partner'
password = 'y+!Gt8Hm4BWPaXPZ'
database = 'partner'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=y = fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})


#b2b prod db
hostname = 'b2b-p-corp-pg.treebo.pr'
username = 'b2buser'
password = 'caTwimEx3'
database = 'b2bapi'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('b2bapi'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]

if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'b2b', 'db_size': fetchvar[0][0]})


#mint prod db
hostname = 'prod-app-stack-pgbouncer-elb-01.treebo.com'
username = 'prowluser'
password = 'caTwimEx3'
database = 'mint'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('mint'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]

if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than"+ str(sizecomparison) + " GB"
   else:
      print "size is greater than"+ str(sizecomparison) + " GB"
      with open('proddbsizes.csv', 'a') as writeFile:
         writer = csv.writer(writeFile)
         fieldnames = ['db_name', 'db_size']
         writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
         print fetchvar[0][0]
         writer.writerow({'db_name': 'mint', 'db_size': fetchvar[0][0]})



#prowl prod db
hostname = 'prod-app-stack-pgbouncer-elb-01.treebo.com'
username = 'prowluser'
password = 'caTwimEx3'
database = 'prowl'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('prowl'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]

if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'prowl', 'db_size': fetchvar[0][0]})

#catalog prod db
hostname = 'pgbouncer-cluster.treebo.com'
username = 'cataloging'
password = 'caTwimEx3'
database = 'cataloging_service'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('cataloging_service'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'cataloging', 'db_size': fetchvar[0][0]})


#its prod db
hostname = 'rms-p-its-rcs-master.treebo.com'
username = 'optimus_write'
password = 'RqazcZL2R2'
database = 'inventory_throttling'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('inventory_throttling'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'inventory_throttling', 'db_size': fetchvar[0][0]})


#koopan prod db
hostname = 'rms-p-rackrate-master-rds.treebo.com'
username = 'optimus_write'
password = 'RqazcZL2R2'
database = 'koopan'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('koopan'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'koopan', 'db_size': fetchvar[0][0]})

#rackrate prod db
hostname = 'rms-p-rackrate-master-rds.treebo.com'
username = 'optimus_write'
password = 'RqazcZL2R2'
database = 'rackrate_db'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('rackrate_db'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'rackrate', 'db_size': fetchvar[0][0]})


#taxation prod db
hostname = 'rms-p-rackrate-master-rds.treebo.com'
username = 'optimus_write'
password = 'RqazcZL2R2'
database = 'taxation'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('taxation'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'taxation', 'db_size': fetchvar[0][0]})


#notification prod db
hostname = 'pgbouncer-cluster.treebo.com'
username = 'treeboadmin'
password = 'RqazcZL2R2'
database = 'notification'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('notification'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than"+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'notification', 'db_size': fetchvar[0][0]})


#growth-relization prod db
hostname = 'web-master-rds.treebo.com'
username = 'treeboadmin'
password = 'caTwimEx3'
database = 'growth'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})

#unirate prod db
hostname = 'rms-p-cm-master.treebo.com'
username = 'treebo_cm'
password = 'PS3XtbZKS3CMqFM2BBxB'
database = 'treebo_cm'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('treebo_cm'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'treebo_cm', 'db_size': fetchvar[0][0]})


#auth prod db
hostname = 'pgb-web-mum.treebohotels.com'
username = 'growth'
password = 'PS3XtbZKS3CMqFM2BBxB'
database = 'treebo_auth'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('treebo_auth'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'treebo_auth', 'db_size': fetchvar[0][0]})


#refund prod db
hostname = 'direct-p-master.treebo.com'
username = 'treeboadmin'
password = 'caTwimEx3'
database = 'auto_refund'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('auto_refund'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'auto_refund', 'db_size': fetchvar[0][0]})


#feedback prod db
hostname = 'direct-p-loyalty-new-master-rds.treebo.com'
username = 'web_loyalty'
password = 'f1L5*5Li83b6'
database = 'feedback_db'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('feedback_db'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'feedback_db', 'db_size': fetchvar[0][0]})



#otp prod db
hostname = 'web-p-loyalty-pg.treebo.pr'
username = 'web_loyalty'
password = 'f1L5*5Li83b6'
database = 'web_otp'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('web_otp'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'web_otp', 'db_size': fetchvar[0][0]})


#payment prod db
hostname = 'pgb-web-mum.treebohotels.com'
username = 'growth'
password = 'XYgs1CnY3rw0'
database = 'payment'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('payment'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'payment', 'db_size': fetchvar[0][0]})


#reconciliation prod db
hostname = 'direct-p-growth-master.treebo.com'
username = 'growth'
password = 'XYgs1CnY3rw0'
database = 'reconciliation'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('reconciliation'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'reconciliation', 'db_size': fetchvar[0][0]})


#reward prod db
hostname = 'web-p-loyalty-pg.treebo.pr'
username = 'web_loyalty'
password = 'f1L5*5Li83b6'
database = 'reward'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size('reward'))")
fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': 'reward', 'db_size': fetchvar[0][0]})


#wallet prod db
hostname = 'web-p-loyalty-pg.treebo.pr'
username = 'web_loyalty'
password = 'f1L5*5Li83b6'
database = 'wallet'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})

#web prod db
hostname = 'pgb-web-mum.treebohotels.com'
username = 'treeboadmin'
password = 'caTwimEx3'
database = 'treebo'
port = 6432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})


#contact_sync prod db
hostname = 'direct-p-loyalty-new-master-rds.treebo.com'
username = 'web_loyalty'
password = 'f1L5*5Li83b6'
database = 'contact_sync'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})

#bumblebee prod db
hostname = 'bumblebee-p-rds-01.cwbpdp5vvep9.ap-south-1.rds.amazonaws.com'
username = 'treeboadmin'
password = 'caTwimEx3'
database = 'bumblebee'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})

#crs-authz prod db
hostname = 'crs-rds-writer.treebo.com'
username = 'crs_user'
password = 'crs_paSSword'
database = 'authz'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
dbsize=fetchvar[0][0].replace('kB','')
print fetchvar[0][0]
if "MB" or "kB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})


#crs-backend prod db
hostname = 'crs-rds-writer.treebo.com'
username = 'crs_user'
password = 'crs_paSSword'
database = 'crs_db'
port = 5432

myConnection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database, port=port)
cur = myConnection.cursor()
cur.execute("SELECT pg_size_pretty(pg_database_size(""'" + database + "'))")

fetchvar= cur.fetchall()
dbsize=fetchvar[0][0].replace('MB','')
dbsize=fetchvar[0][0].replace('GB','')
print fetchvar[0][0]
if "MB" in fetchvar[0][0]:
    print "size is fine"
else:
   print "need to check the size"
   if (int(dbsize)< sizecomparison):
       print "size is lesser than "+ str(sizecomparison) + " GB"
   else:
       print "size is greater than "+ str(sizecomparison) + " GB"
       with open('proddbsizes.csv', 'a') as writeFile:
          writer = csv.writer(writeFile)
          fieldnames = ['db_name', 'db_size']
          writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
          writer.writerow({'db_name': database, 'db_size': fetchvar[0][0]})

writeFile.close()



#for sending the mail 
SMTPserver = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_PORT = 587

sender = 'Prod Scripts <dev@treebohotels.com>'

destination = ['kaddy@treebohotels.com','arun.midha@treebohotels.com','mayank.khandelwal@treebohotels.com','abhinav.nigam@treebohotels.com','soumya.das@treebohotels.com']

USERNAME = "AKIAYZ7GE5SNATIB23OR"
PASSWORD = "AoC9Wwy92HhTiO3dFtF8B8PuvnA1zKTgf4JgXcwaab9i"

  # typical values for text_subtype are plain, html, xml
text_subtype = 'plain'
body="checked for:marvin,b2b,mint,prowl,catalog,its,koopan,rackrate,taxation,notification,growth,CM,auth,refund,feedback,otp,payment,reconciliation,reward,wallet,web,contact_sync,bumblebee,authz,crs-backend,hms"
subject="Urgent:Prod db sizes greater than 10 GB"
try:
      msg = MIMEMultipart()
      #msg = MIMEText(content, text_subtype)
      msg['Subject']=       subject
      msg['From']   = sender # some SMTP servers will do this automatically, not all

      msg.attach(MIMEText(body, 'plain'))
      part = MIMEBase('application', "octet-stream")
      part.set_payload(open("proddbsizes.csv", "rb").read())
      Encoders.encode_base64(part)
      part.add_header('Content-Disposition', 'attachment; filename="proddbsizes.csv"')

      msg.attach(part)
      conn = SMTP(SMTPserver)
      conn.set_debuglevel(False)
      conn.login(USERNAME, PASSWORD)
      try:
          conn.sendmail(sender, destination, msg.as_string())
      finally:
          conn.quit()

except Exception, exc:
      sys.exit( "mail failed; %s" % str(exc) )

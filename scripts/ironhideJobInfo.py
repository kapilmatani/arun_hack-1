import requests
import json
from requests.auth import  HTTPBasicAuth

def send_jenkins_job_api():
    jenkins_job_url = 'http://ci.treebo.com:8080/job/crs-s-cybertron-ironhide-build-ecs/lastStableBuild/api/json'
    auth_token = '4Ep;hT8U7[:H~32a'
    user_name = 'arun.midha'
    response = requests.get(jenkins_job_url,auth=HTTPBasicAuth(user_name,auth_token))
    return response

def send_slack_alert(branch_name ,deployer_name ):
    slack_webhhook = 'https://hooks.slack.com/services/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'
    payload = str({"text": "{} branch has been deployed by {} in the latest stable build".format(branch_name,deployer_name)})
    requests.post(slack_webhhook,payload)
    print("sent")
response = send_jenkins_job_api()
response = json.loads(response.text)

for dict in response["actions"]:
    if '_class' in dict:
        if dict["_class"] == 'hudson.model.CauseAction':
            if dict["causes"][0]["_class"] == "hudson.model.Cause$UserIdCause":
                deployer_name = dict["causes"][0]['userId']

        elif dict["_class"] == 'hudson.model.ParametersAction':
            branch_name = dict["parameters"][0]["value"]

send_slack_alert(branch_name,deployer_name)
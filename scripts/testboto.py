import boto3
import json

from datetime import datetime, timedelta,date
from json import JSONEncoder
from functools import singledispatch

@singledispatch
def to_serializable(obj):
    """Used by default."""
    if hasattr(obj, "__json__"):
        return obj.__json__()
    return str(obj)


@to_serializable.register(datetime)
def ts_datetime(obj):
    return obj.isoformat()


@to_serializable.register(date)
def ts_date(obj):
    return obj.isoformat()




client = boto3.client('cloudwatch',region_name='ap-south-1')

StartTime=datetime.utcnow() - timedelta(seconds=86400)
EndTime=datetime.utcnow()

stat1=client.get_metric_statistics(
    Namespace='AWS/Redshift',
    MetricName='CPUUtilization',
    Dimensions=[{'Name':'NodeID','Value':'Compute-0'},
                {'Name':'ClusterIdentifier','Value':'dwh'}
                ],
    StartTime=datetime.utcnow() - timedelta(seconds=86400),
    EndTime=datetime.utcnow(),
    Period=60,
    Statistics=['Maximum']
    )
stat1 = json.dumps(stat1,default=to_serializable)
print(stat1)
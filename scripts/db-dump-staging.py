import boto3
import json
import os

session = boto3.session.Session()
client = session.client(
        service_name='secretsmanager',
        region_name='ap-southeast-1',
    )

crs_response = client.get_secret_value(
            SecretId="treebo/staging/apps/crs/postgres"
        )
res_crs = json.loads(crs_response['SecretString'])
crs_user = res_crs['username']
crs_password = res_crs['password']
crs_port = res_crs['port']
crs_host = res_crs['host']

crs_service = ['crs','ar_module','ups','company_profiles','pos','authn','authz','treebo_platform','group_profiles']
for i in crs_service:
    print(i)
    get_secret_value_response = client.get_secret_value(
            SecretId="treebo/staging/apps/"+str(i)+"/postgres"
        )
    #print(get_secret_value_response['SecretString'])
    res = json.loads(get_secret_value_response['SecretString'])
    crs_db_name = res['dbname']
    os.system('''PGPASSWORD=''' + crs_password + ''' pg_dump -h ''' + crs_host + ''' -p ''' + crs_port + ''' -d ''' + crs_db_name + ''' -U ''' + crs_user + ''' | aws s3 cp - s3://treebo-db-dump-stage/''' + i +'''_backup-$(date "+%Y-%m-%d").tar''')


rms_response = client.get_secret_value(
            SecretId="treebo/staging/apps/rackrate/postgres"
        )
res_rms = json.loads(rms_response['SecretString'])
rms_user = res_rms['username']
rms_password = res_rms['password']
rms_port = res_rms['port']
rms_host = res_rms['host']

rms_service = ['catalog','rackrate','taxation','inventory_throttling','staff_management','interface_exchange','tenant_gateway_service']
for i in rms_service:
    print(i)
    get_secret_value_response = client.get_secret_value(
            SecretId="treebo/staging/apps/"+str(i)+"/postgres"
        )
    #print(get_secret_value_response['SecretString'])
    res = json.loads(get_secret_value_response['SecretString'])
    rms_db_name = res['dbname']
    os.system('''PGPASSWORD=''' + rms_password + ''' pg_dump --cluster 9.6/main -h ''' + rms_host + ''' -p ''' + rms_port + ''' -d ''' + rms_db_name + ''' -U ''' + rms_user + ''' | aws s3 cp - s3://treebo-db-dump-stage/''' + i +'''_backup-$(date "+%Y-%m-%d").tar''')

crstest_response = client.get_secret_value(
            SecretId="treebo/staging/apps/crstest/postgres"
        )
res_crstest = json.loads(crstest_response['SecretString'])
crstest_user = res_crstest['username']
crstest_password = res_crstest['password']
crstest_port = res_crstest['port']
crstest_host = res_crstest['host']

crs_service = ['crstest']
for i in crs_service:
    print(i)
    get_secret_value_response = client.get_secret_value(
            SecretId="treebo/staging/apps/"+str(i)+"/postgres"
        )
    #print(get_secret_value_response['SecretString'])
    res = json.loads(get_secret_value_response['SecretString'])
    crstest_db_name = res['dbname']
    os.system('''PGPASSWORD=''' + crstest_password + ''' pg_dump -h ''' + crstest_host + ''' -p ''' + crstest_port + ''' -d ''' + crstest_db_name + ''' -U ''' + crstest_user + ''' | aws s3 cp - s3://treebo-db-dump-stage/''' + i +'''_backup-$(date "+%Y-%m-%d").tar''')


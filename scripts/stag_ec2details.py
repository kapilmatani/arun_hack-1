import boto3    
import requests
import os
ec2client = boto3.client('ec2','ap-southeast-1')
webhook_url = "https://hooks.slack.com/services/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"
stri = "Name \t \t \t ip  \n"
response = ec2client.describe_instances()
for reservation in response["Reservations"]:
    for instance in reservation["Instances"]:
        for tag in instance['Tags']:
            if 'Name' in tag['Key']:
                if ( tag['Value'] == 'docker-hub' or tag['Value'] == 'gateway-01' or tag['Value'] == 'shared-s-pgbouncer-01a' or tag['Value'] == 'devops-s-pypi-01a' or tag['Value'] == 'devops-s-jenkins-01a' ) or tag['Value'].find('-asg') != -1:
                    continue
                else:
                    #print(tag['Value'])
                    #os.system('''ssh -i ~/.ssh/conman_id_rsa conman@'''+ instance['PrivateIpAddress'] +''' "clamd --version" ''')
                    stri = stri + tag['Value'] + "\t"
                    stri = stri + instance['PrivateIpAddress'] + "\n"
            #if 'app' in tag['Key']:
                #stri = stri + tag['Value'] + "\n"
payload = str({"text": stri})
requests.post(webhook_url, payload)
print(stri)

